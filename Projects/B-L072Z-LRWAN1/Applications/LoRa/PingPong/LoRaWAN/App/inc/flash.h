/**
  ******************************************************************************
  * @file    flash.h
  * @author  A.Kabanov
  * @brief   contains function for work with flash
 
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FLASH_H__
#define __FLASH_H__

#ifdef __cplusplus
extern "C" {
#endif
/* Includes ------------------------------------------------------------------*/
#include "hw.h"
 
/**
  * @brief calculate how many bytes should be in one message from necksensor
  * @param non
  * @retval number of byte to receive
  */
uint32_t flashBuffSize(void);

/**
  * @brief erase user flash memory
  * @param non
  * @retval true if ok, false if error
  */
bool flashInit(void);

/**
  * @brief Program the user Flash area word by word
  * (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR)
  * @param Buffer - data Array to store to flash
  * @param size - size of data Array
  * @retval true if ok, false if error
  */

bool flashWrite(uint8_t* Buffer, uint32_t size);

/**
  * @brief write the end address of saved data into the flash 
  * (address is FLASH_USER_START_ADDR - 4)
  * @param 
  * @param 
  * @retval true if ok, false if error
  */
bool flashWriteEndAddress(void);

/**
  * @brief restore the saved end address of saved data from the flash 
  * (address is FLASH_USER_START_ADDR - 4)
  * @param 
  * @param 
  * @retval true if ok, false if error
  */
bool flashSetAddr(void);

/**
  * @brief convert data in flash into strings for csv 
  *  "x","y","z"\n\r 
  *   1,2,3\n\r
  *   -1,-2,-3\n\r
  * @param NoN
  * @retval string to send to PC
  */
char* flashGetStr(void);



#ifdef __cplusplus
}
#endif

#endif /* __FLASH_H__ */
/*****************************END OF FILE****/

