/*!
 * \file      main.c
 *
 * \brief     Ping-Pong implementation
 *
 * \copyright Revised BSD License, see section \ref LICENSE.
 *
 * \code
 *                ______                              _
 *               / _____)             _              | |
 *              ( (____  _____ ____ _| |_ _____  ____| |__
 *               \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 *               _____) ) ____| | | || |_| ____( (___| | | |
 *              (______/|_____)_|_|_| \__)_____)\____)_| |_|
 *              (C)2013-2017 Semtech
 *
 * \endcode
 *
 * \author    Miguel Luis ( Semtech )
 *
 * \author    Gregory Cristian ( Semtech )
 */
/**
  ******************************************************************************
  * @file    main.c
  * @author  MCD Application Team
  * @brief   this is the main!
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2018 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <string.h>
#include "hw.h"
#include "radio.h"
#include "timeServer.h"
#include "low_power_manager.h"
#include "vcom.h"
#include "flash.h"

#define SENSOR_SN                                   25
#define TX_FREQUENCY                               869120000  // Hz    
#define RAW_DATA_FREQUENCY                         864900000   //Hz


#define TX_OUTPUT_POWER                             14        // dBm

#if defined( USE_MODEM_LORA )

#define LORA_BANDWIDTH                              0         // [0: 125 kHz,
                                                              //  1: 250 kHz,
                                                              //  2: 500 kHz,
                                                              //  3: Reserved]
#define LORA_SPREADING_FACTOR                       8         // [SF7..SF12]
#define LORA_SF7                                    7
#define LORA_CODINGRATE                             1         // [1: 4/5,
                                                              //  2: 4/6,
                                                              //  3: 4/7,
                                                              //  4: 4/8]
#define LORA_PREAMBLE_LENGTH                        8         // Same for Tx and Rx
#define LORA_SYMBOL_TIMEOUT                         5         // Symbols
#define LORA_FIX_LENGTH_PAYLOAD_ON                  false
#define LORA_IQ_INVERSION_ON                        false

#elif defined( USE_MODEM_FSK )

#define FSK_FDEV                                    25000     // Hz
#define FSK_DATARATE                                50000     // bps
#define FSK_BANDWIDTH                               50000     // Hz
#define FSK_AFC_BANDWIDTH                           83333     // Hz
#define FSK_PREAMBLE_LENGTH                         5         // Same for Tx and Rx
#define FSK_FIX_LENGTH_PAYLOAD_ON                   false

#else
    #error "Please define a modem in the compiler options."
#endif

typedef enum
{
    LOWPOWER,
    RX,
    RX_TIMEOUT,
    RX_ERROR,
    TX,
    TX_TIMEOUT,
}States_t;

#define RX_TIMEOUT_VALUE                            5000
#define BUFFER_SIZE                                 140 // Define the payload size here
#define LED_PERIOD_MS               200

#define LEDS_OFF   do{ \
                   LED_Off( LED_BLUE ) ;   \
                   LED_Off( LED_RED ) ;    \
                   LED_Off( LED_GREEN1 ) ; \
                   LED_Off( LED_GREEN2 ) ; \
                   } while(0) ;

const uint8_t PingMsg[] = "PING";
const uint8_t PongMsg[] = "PONG";

uint16_t BufferSize = BUFFER_SIZE;
union {
      uint32_t address;
      uint8_t Buffer[BUFFER_SIZE];
} LoRa;


States_t State = LOWPOWER;

int8_t RssiValue = 0;
int8_t SnrValue = 0;
uint32_t RxCounter = 0;
uint16_t errorCounter = 0;
bool startLogging = false;
bool endLogging = false;
TimerTime_t time1 = 0,time2 = 0; //TimerTime_t TimerGetCurrentTime( void )
bool noError = false;
uint32_t button = 1, buttonPrev = 1, buttonCnt = 0;
uint32_t frequencies[] = {864780000,864100000,864300000,86450000,864640000};
uint8_t bands = sizeof(frequencies)/sizeof(uint32_t);


 /* Led Timers objects*/
static  TimerEvent_t timerLed;

/* Private function prototypes -----------------------------------------------*/
/*!
 * Radio events function pointer
 */
static RadioEvents_t RadioEvents;

/*!
 * \brief Function to be executed on Radio Tx Done event
 */
void OnTxDone( void );

/*!
 * \brief Function to be executed on Radio Rx Done event
 */
void OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );

/*!
 * \brief Function executed on Radio Tx Timeout event
 */
void OnTxTimeout( void );

/*!
 * \brief Function executed on Radio Rx Timeout event
 */
void OnRxTimeout( void );

/*!
 * \brief Function executed on Radio Rx Error event
 */
void OnRxError( void );

/*!
 * \brief Function executed on when led timer elapses
 */
static void OnledEvent( void* context );
/**
 * Main application entry point.
 */
int main( void )
{
    HAL_Init( );

    SystemClock_Config( );

    //DBG_Init( );

    HW_Init( );  
    
    
    /*Disbale Stand-by mode*/
    LPM_SetOffMode(LPM_APPLI_Id , LPM_Disable );
    

    /* Led Timers*/
   // TimerInit(&timerLed, OnledEvent);   
   // TimerSetValue( &timerLed, LED_PERIOD_MS);

   // TimerStart(&timerLed );

    // Radio initialization
    RadioEvents.TxDone = OnTxDone;
    RadioEvents.RxDone = OnRxDone;
    RadioEvents.TxTimeout = OnTxTimeout;
    RadioEvents.RxTimeout = OnRxTimeout;
    RadioEvents.RxError = OnRxError;
    

    Radio.Init( &RadioEvents );

    Radio.SetChannel( frequencies[SENSOR_SN%bands] );

#if defined( USE_MODEM_LORA )

    Radio.SetTxConfig( MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
                                   LORA_SPREADING_FACTOR, LORA_CODINGRATE,
                                   LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
                                   true, 0, 0, LORA_IQ_INVERSION_ON, 3000 );

    Radio.SetRxConfig( MODEM_LORA, LORA_BANDWIDTH, LORA_SPREADING_FACTOR,
                                   LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
                                   LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON,
                                   0, true, 0, 0, LORA_IQ_INVERSION_ON, true );
    Radio.SetPublicNetwork(true);
    Radio.SetMaxPayloadLength(MODEM_LORA,BUFFER_SIZE);

#elif defined( USE_MODEM_FSK )

    Radio.SetTxConfig( MODEM_FSK, TX_OUTPUT_POWER, FSK_FDEV, 0,
                                  FSK_DATARATE, 0,
                                  FSK_PREAMBLE_LENGTH, FSK_FIX_LENGTH_PAYLOAD_ON,
                                  true, 0, 0, 0, 3000 );

    Radio.SetRxConfig( MODEM_FSK, FSK_BANDWIDTH, FSK_DATARATE,
                                  0, FSK_AFC_BANDWIDTH, FSK_PREAMBLE_LENGTH,
                                  0, FSK_FIX_LENGTH_PAYLOAD_ON, 0, true,
                                  0, 0,false, true );

#else
    #error "Please define a frequency band in the compiler options."
#endif
    
//    while(1)
//    {
//      LoRa.Buffer[0] = 0x03;  //start sending raw data from accelerometer
//      LoRa.Buffer[1] = SENSOR_ADDR;
//      Radio.SetChannel( TX_FREQUENCY );
//      DelayMs( 10 );
//      Radio.Send( LoRa.Buffer, 32 );
//      DelayMs(1000);
//      Radio.SetChannel( RF_FREQUENCY );
//      DelayMs( 10 );
//      Radio.Send( LoRa.Buffer, 32 );
//      DelayMs(1000);
//    }
    if(BSP_PB_GetState(BUTTON_KEY) == 0){
      LED_On(LED_RED2);  
      noError = flashInit();  //clean flash if button is pressed
      while(BSP_PB_GetState(BUTTON_KEY) == 0);
      Radio.Rx( RX_TIMEOUT_VALUE );
    } 
    LED_Off(LED_RED2);  
              
    
    

    while( 1 )
    {
        switch( State )
        {
        case RX:
            
            if(startLogging == false)
            {
              if((BufferSize == 32)&&(LoRa.address == SENSOR_SN))
              { 
                LED_On(LED_GREEN);
                startLogging = true;
                Radio.SetChannel( TX_FREQUENCY );
                LoRa.Buffer[0] = 0x03;  //start sending raw data from accelerometer
                LoRa.Buffer[1] = SENSOR_SN;
                DelayMs( 10 );
                Radio.Send( LoRa.Buffer, 2 );
              }  
            }
            else
            {
                Radio.Rx( RX_TIMEOUT_VALUE );
                if((BufferSize == flashBuffSize())&&(startLogging == true))
                {       //receive log  
                  LED_Toggle(LED_RED2); 
                  RxCounter++;
                  noError = flashWrite(LoRa.Buffer,BufferSize);
                }
            }
            
            State = LOWPOWER;
            break;
        case TX:
            // Indicates on a LED that we have sent a PING [Master]
            // Indicates on a LED that we have sent a PONG [Slave]
            //GpioWrite( &Led2, GpioRead( &Led2 ) ^ 1 );
            Radio.SetChannel( RAW_DATA_FREQUENCY );
            Radio.SetRxConfig( MODEM_LORA, LORA_BANDWIDTH, LORA_SF7,
                                   LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
                                   LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON,
                                   0, true, 0, 0, LORA_IQ_INVERSION_ON, true );
            DelayMs( 10 );
            Radio.Rx( RX_TIMEOUT_VALUE );
            State = LOWPOWER;
            break;
        case RX_TIMEOUT:
            LED_Toggle(LED_BLUE);
            if(startLogging == true)
            {
              endLogging = true;
              startLogging = false;
              LED_On(LED_BLUE);
              flashWriteEndAddress();
            }
            else Radio.Rx( RX_TIMEOUT_VALUE );
            State = LOWPOWER;
            break;
        case RX_ERROR:
            LED_Toggle(LED_RED1);
            Radio.Rx( RX_TIMEOUT_VALUE );
            State = LOWPOWER;
            break;
        case TX_TIMEOUT:
            Radio.Rx( RX_TIMEOUT_VALUE );
            State = LOWPOWER;
            break;
        case LOWPOWER:
        default:
            // Set low power
            break;
        }//end_switch(State)
/*
        DISABLE_IRQ( );
        // if an interupt has occured after __disable_irq, it is kept pending 
        // and cortex will not enter low power anyway  
        if (State == LOWPOWER)
        {
#ifndef LOW_POWER_DISABLE
          LPM_EnterLowPower( );
#endif
        }
        ENABLE_IRQ( );
*/  
        button  = BSP_PB_GetState(BUTTON_KEY);
        if((startLogging == false)&&(button != buttonPrev)&&(button == 0))
        { 
          if(endLogging == false)flashSetAddr();
          /*send data to USB*/
          flashGetStr();                 
        }//end_if
        buttonPrev = button;
    }//end_while(1)
}

void OnTxDone( void )
{
    Radio.Sleep( );
    State = TX;
    //PRINTF("OnTxDone\n\r");
}

void OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
    Radio.Sleep( );
    BufferSize = size;
    memcpy( LoRa.Buffer, payload, BufferSize );
    RssiValue = rssi;
    SnrValue = snr;
    State = RX;

    //PRINTF("OnRxDone\n\r");
    //PRINTF("RssiValue=%d dBm, SnrValue=%d\n\r", rssi, snr);
}

void OnTxTimeout( void )
{
    Radio.Sleep( );
    State = TX_TIMEOUT;

    //PRINTF("OnTxTimeout\n\r");
}

void OnRxTimeout( void )
{
    Radio.Sleep( );
    State = RX_TIMEOUT;
    //PRINTF("OnRxTimeout\n\r");
}

void OnRxError( void )
{
    Radio.Sleep( );
    State = RX_ERROR;
    //PRINTF("OnRxError\n\r");
}

static void OnledEvent( void* context )
{
//  LED_Toggle( LED_BLUE ) ; 
//  LED_Toggle( LED_RED1 ) ; 
//  LED_Toggle( LED_RED2 ) ; 
//  LED_Toggle( LED_GREEN ) ;   

  TimerStart(&timerLed );
}

