/**
  ******************************************************************************
  * @file    flash.c
  * @author  A.Kabanoff
  * @brief   write and read from flash memory
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "flash.h"
/** @addtogroup FLASH_Program
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/*
   stm32l073xx.h
   #define FLASH_BASE             (0x08000000UL) //!< FLASH base address in the alias region *
   stm32lxx_hal_flash.h
   #define FLASH_SIZE                (uint32_t)((*((uint32_t *)FLASHSIZE_BASE)&0xFFFF) * 1024U)
   #define FLASH_PAGE_SIZE           ((uint32_t)128U)  //!< FLASH Page Size in bytes *

   #define FLASH_END                 (FLASH_BASE + FLASH_SIZE - 1)   //!< FLASH end address in the alias region 

   #if defined (STM32L071xx) || defined (STM32L072xx) || defined (STM32L073xx) 
    || defined (STM32L081xx) || defined (STM32L082xx) || defined (STM32L083xx)
   #define FLASH_BANK2_BASE          (FLASH_BASE + (FLASH_SIZE >> 1)) //!< FLASH BANK2 base address in the alias region 
   #define FLASH_BANK1_END           (FLASH_BANK2_BASE - 1)           //!< Program end FLASH BANK1 address 
   #define FLASH_BANK2_END           (FLASH_END)                      //!< Program end FLASH BANK2 address 
   #endif  
*/
#define FLASH_USER_START_ADDR   (FLASH_BASE + FLASH_PAGE_SIZE * 512)             /* Start @ of user Flash area */
#define FLASH_USER_END_ADDR     (FLASH_USER_START_ADDR + FLASH_PAGE_SIZE * 1024)   /* End @ of user Flash area 128b * 1024 = 131072 */

#define PAGE_SAMPLES 28  //defines how many samples per second
#define REM_BYTES (PAGE_SAMPLES%2?PAGE_SAMPLES/2+1:PAGE_SAMPLES/2)
#define REM_SIGNS (PAGE_SAMPLES%8?(PAGE_SAMPLES/8 + 1):PAGE_SAMPLES/8)


/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

__IO uint32_t data32 = 0 , MemoryProgramStatus = 0;

volatile uint32_t debug1 = 0,debug2= 0,debug3 = 0,debug4 = 0;

struct sendStruct{
  uint8_t rawX[PAGE_SAMPLES];  
  uint8_t rawY[PAGE_SAMPLES];
  uint8_t rawZ[PAGE_SAMPLES];
  uint8_t rawXrem[REM_BYTES];  
  uint8_t rawYrem[REM_BYTES];
  uint8_t rawZrem[REM_BYTES];
  uint8_t rawXsign[REM_SIGNS];
  uint8_t rawYsign[REM_SIGNS];
  uint8_t rawZsign[REM_SIGNS];
};                         // 3*28 + 3*14 + 3*4 = 138 bytes in message
#define REC_BUF (sizeof(struct sendStruct))

uint32_t Address = FLASH_USER_START_ADDR, PAGEError = 0;
uint32_t flashSlot = REC_BUF%4?REC_BUF + 4 - REC_BUF%4:REC_BUF;
uint32_t flashSlotPtr = FLASH_USER_START_ADDR;

/*Variable used for Erase procedure*/
static FLASH_EraseInitTypeDef EraseInitStruct;

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/
  
/* Public functions ---------------------------------------------------------*/ 

/**
  * @brief calculate how many bytes should be in one message from necksensor
  * @param non
  * @retval number of byte to receive
  */
uint32_t flashBuffSize(void)
{
  return REC_BUF;
}

/**
  * @brief erase user flash memory
  * @param non
  * @retval true if ok, false if error
  */
bool flashInit(void)
{
  /* Unlock the Flash to enable the flash control register access *************/
  HAL_FLASH_Unlock();

  /* Erase the user Flash area
    (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) ***********/

  /* Fill EraseInit structure*/
  EraseInitStruct.TypeErase   = FLASH_TYPEERASE_PAGES;
  EraseInitStruct.PageAddress = FLASH_USER_START_ADDR - FLASH_PAGE_SIZE;
  EraseInitStruct.NbPages     = (FLASH_USER_END_ADDR - FLASH_USER_START_ADDR)/FLASH_PAGE_SIZE + 1;

  HAL_StatusTypeDef erase = HAL_FLASHEx_Erase(&EraseInitStruct, &PAGEError);
  
  /* Lock the Flash to disable the flash control register access (recommended
     to protect the FLASH memory against possible unwanted operation) *********/
  HAL_FLASH_Lock();
  if (erase!= HAL_OK)return false;
      /*
      Error occurred while page erase.
      User can add here some code to deal with this error.
      PAGEError will contain the faulty page and then to know the code error on this page,
      user can call function 'HAL_FLASH_GetError()'
    */
  return true;
  
}

/**
  * @brief Program the user Flash area word by word
  * (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR)
  * @param Buffer - data Array to store to flash
  * @param size - size of data Array
  * @retval true if ok, false if error
  */

bool flashWrite(uint8_t* Buffer, uint32_t size)
{
  uint8_t *progArrStart;
  uint32_t index = 0;
  bool noError = true;
  uint32_t endAddress = Address+size;
  if(endAddress > FLASH_USER_END_ADDR)return false;
  if(!(progArrStart = (uint8_t *)malloc(size)))return false;
  memcpy(progArrStart,Buffer,size);
  
  /* Unlock the Flash to enable the flash control register access *************/
  
  HAL_FLASH_Unlock();
  while (Address < endAddress)
  {
    if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, Address, *(uint32_t*)(progArrStart+index)) == HAL_OK)
    {
      Address += 4;
      index +=4;
    }
    else noError = false;
  }
  free(progArrStart);
  /* Lock the Flash to disable the flash control register access (recommended
     to protect the FLASH memory against possible unwanted operation) *********/
  HAL_FLASH_Lock();
  return noError;
}
/**
  * @brief write the end address of saved data into the flash 
  * (address is FLASH_USER_START_ADDR - 4)
  * @param 
  * @param 
  * @retval true if ok, false if error
  */
bool flashWriteEndAddress(void)
{
  bool noError = true;
  HAL_FLASH_Unlock();
  if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, FLASH_USER_START_ADDR - 4, Address) != HAL_OK)noError = false;
  HAL_FLASH_Lock();
  return noError;
}
/**
  * @brief restore the saved end address of saved data from the flash 
  * (address is FLASH_USER_START_ADDR - 4)
  * @param 
  * @param 
  * @retval true if ok, false if error
  */
bool flashSetAddr(void)
{  
  Address  = *(__IO uint32_t *)(FLASH_USER_START_ADDR - 4); 
  return true;
}
/**
  * @brief convert data in flash into strings for csv 
  *  "x","y","z"\n\r 
  *   1,2,3\n\r
  *   -1,-2,-3\n\r
  * @param NoN
  * @retval string to send to PC
        for(int ii = 0; ii< PAGE_SAMPLES;ii++){
          X[ii] = inData->rawX[ii];
          if(ii%2 == 0) X[ii] |= ((uint16_t)(inData->rawXrem[ii/2])<<8)&0x0f00; //low nimble
          else X[ii] |= ((uint16_t)(inData->rawXrem[ii/2])<<4)&0x0f00;//high nimble
          if((inData->rawXsign[ii/8])&(1<<ii%8))X[ii] |= 0xF000;  //negativ	
          Y[ii] = inData->rawY[ii];
          if(ii%2 == 0) Y[ii] |= ((uint16_t)(inData->rawYrem[ii/2])<<8)&0x0f00; //low nimble
          else Y[ii] |= ((uint16_t)(inData->rawYrem[ii/2])<<4)&0x0f00;//high nimble
          if((inData->rawYsign[ii/8])&(1<<ii%8))Y[ii] |= 0xF000;  //negativ	
          Z[ii] = inData->rawZ[ii];
          if(ii%2 == 0) Z[ii] |= ((uint16_t)(inData->rawZrem[ii/2])<<8)&0x0f00; //low nimble
          else Z[ii] |= ((uint16_t)(inData->rawZrem[ii/2])<<4)&0x0f00;//high nimble
          if((inData->rawZsign[ii/8])&(1<<ii%8))Z[ii] |= 0xF000;  //negativ	
          fprintf(rawData_file,"%d,%d,%d\n",X[ii],Y[ii],Z[ii]);					  
	}
  */
char errorStr[] = "Error\n\r";
//char str[80];
union {
  uint32_t words[REC_BUF/4 + 1];
  struct sendStruct inData; 
} flashData;
//uint32_t flashDataPtr = 0;
//uint32_t strCounter = 0;
int16_t X[PAGE_SAMPLES]; 
int16_t Y[PAGE_SAMPLES];
int16_t Z[PAGE_SAMPLES];
bool stopString = false;


char* flashGetStr(void)
{
  PRINTF("\"X\",\"Y\",\"Z\"\n\r");  //header
  while(flashSlotPtr < Address){
    for(int i = 0;i < REC_BUF/4 + 1;i++){  //read 1 sec data from flash 
      flashData.words[i]  = *(__IO uint32_t *)(flashSlotPtr + i*4);
    }
    for(int ii = 0; ii< PAGE_SAMPLES;ii++){
      X[ii] = flashData.inData.rawX[ii];
      if(ii%2 == 0) X[ii] |= ((uint16_t)(flashData.inData.rawXrem[ii/2])<<8)&0x0f00; //low nimble
      else X[ii] |= ((uint16_t)(flashData.inData.rawXrem[ii/2])<<4)&0x0f00;//high nimble
      if((flashData.inData.rawXsign[ii/8])&(1<<ii%8))X[ii] |= 0xF000;  //negativ	
      Y[ii] = flashData.inData.rawY[ii];
      if(ii%2 == 0) Y[ii] |= ((uint16_t)(flashData.inData.rawYrem[ii/2])<<8)&0x0f00; //low nimble
      else Y[ii] |= ((uint16_t)(flashData.inData.rawYrem[ii/2])<<4)&0x0f00;//high nimble
      if((flashData.inData.rawYsign[ii/8])&(1<<ii%8))Y[ii] |= 0xF000;  //negativ	
      Z[ii] = flashData.inData.rawZ[ii];
      if(ii%2 == 0) Z[ii] |= ((uint16_t)(flashData.inData.rawZrem[ii/2])<<8)&0x0f00; //low nimble
      else Z[ii] |= ((uint16_t)(flashData.inData.rawZrem[ii/2])<<4)&0x0f00;//high nimble
      if((flashData.inData.rawZsign[ii/8])&(1<<ii%8))Z[ii] |= 0xF000;  //negativ	
      DelayMs( 2 );
      PRINTF("%d,%d,%d,\n\r",X[ii],Y[ii],Z[ii]);
    }     
    
    flashSlotPtr += flashSlot;
    
  }
  flashSlotPtr = FLASH_USER_START_ADDR;  //prepare for next transaction
  return errorStr;
}
/* Check if the programmed data is OK
      MemoryProgramStatus = 0: data programmed correctly
      MemoryProgramStatus != 0: number of words not programmed correctly 
  Address = FLASH_USER_START_ADDR;
  MemoryProgramStatus = 0x0;

  while (Address < FLASH_USER_END_ADDR)
  {
    data32 = *(__IO uint32_t *)Address;

    if (data32 != DATA_32)
    {
      MemoryProgramStatus++;
    }
    Address = Address + 4;
  }
******/  
/**
  * @}
  */

/*****************************END OF FILE****/